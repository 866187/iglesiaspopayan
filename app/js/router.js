angular.module("app").config(function($routeProvider) {


  $routeProvider.when('/login', {
    templateUrl: 'login.html',
    controller: 'LoginController'
  });

  $routeProvider.when('/iglesia', {
    templateUrl: 'iglesia.html',
    controller: 'IglesiasController'
  });

  $routeProvider.when('/sitios', {
    templateUrl: 'sitios.html',
    controller: 'SitiosController'
  });

  $routeProvider.when('/restaurante', {
    templateUrl: 'restaurante.html',
    controller: 'RestauranteController',

  });
  $routeProvider.when('/clima', {
    templateUrl: 'clima.html',
    controller: 'CLimaController',

  });

  $routeProvider.otherwise({ redirectTo: '/login' });

});
